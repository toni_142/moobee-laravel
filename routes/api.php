<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
 * Public routes (doesn't require login)
 */
Route::post('/register', 'UserController@register')->name('register');
Route::post('/login', 'UserController@login')->name('login');
Route::get('/home-info', 'Controller@getHomeInfo');

Route::post('/webhooks/bitbucket', function () {
    try {
        shell_exec(base_path() . 'deploy.sh');
    } catch (Exception $e) {
        return Response('Script execution failed, reason: ' . $e->getMessage());
    }

    return Response('Script executed successfully');
});

/*
 * Protected routes (requires login)
 */
Route::middleware('auth:api')->group(function () {
    Route::resource('/watchlist', 'WatchlistController');

    Route::get('/timeline', 'MoobController@getFollowingMoobs');
    Route::get('/notifications', 'NotificationsController@index');

    Route::get('/moobs/check', 'MoobController@canSendMoob');
    Route::get('/moobs/film/{filmId}', 'MoobController@getFilmMoobs');
    Route::post('/moobs/like/{moobId}', 'MoobController@likeMoob');
    Route::post('/moobs/unlike/{moobId}', 'MoobController@unlikeMoob');
    Route::post('/moobs/spoilers/{moobId}', 'UserSpoilsMoobController@store');
    Route::get('/moobs/spoilers/{moobId}', 'UserSpoilsMoobController@show');
    Route::delete('/moobs/spoilers/{moobId}', 'UserSpoilsMoobController@destroy');
    Route::resource('/moobs', 'MoobController');

    Route::get('/movie/{id}/info', 'MoviesStatsController@show');
    Route::get('/movie/{filmId}/following/viewings', 'UserController@getFollowersWhoWatchedFilm');
    Route::get('/movie/{filmId}/following/likes', 'UserController@getFollowersWhoLikedFilm');

    Route::post('/followers/follow/{userId}', 'UserFollowsController@follow');
    Route::post('/followers/unfollow/{userId}', 'UserFollowsController@unfollow');

    Route::get('/user/{id}/watchlist', 'UserController@getUserWatchlist');
    Route::get('/user/{id}/moobs', 'UserController@getUserMoobs');

    Route::get('/profile/{userId}', 'UserController@show');

    Route::get('/search/users/{query}', 'UserController@search');
});
