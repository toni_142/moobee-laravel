#!/bin/bash
cd /home/moobee/moobee-laravel/
git pull origin master
composer install --no-interaction --prefer-dist --optimize-autoloader
php artisan migrate --force --env=production
php artisan queue:restart