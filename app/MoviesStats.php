<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoviesStats extends Model
{
    public $timestamps = false;

    protected $table = 'movies_stats';

    protected $fillable = ['film_id', 'times_seen', 'moobs_sent', 'total_likes'];

    protected $hidden = ['id'];
}
