<?php

namespace App\Providers;

use App\Events\MoobDeleted;
use App\Events\MoobSaved;
use App\Events\UserFollowsCreated;
use App\Events\UserWatchlistDeleted;
use App\Events\UserWatchlistSaved;
use App\Listeners\SendPushOnUserFollowsCreated;
use App\Listeners\UpdateMovieStatsTableOnMoobDelete;
use App\Listeners\UpdateMovieStatsTableOnMoobSave;
use App\Listeners\UpdateMovieStatsTableOnWatchlistDelete;
use App\Listeners\UpdateMovieStatsTableOnWatchlistSave;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserWatchlistSaved::class => [
            UpdateMovieStatsTableOnWatchlistSave::class,
        ],
        UserWatchlistDeleted::class => [
            UpdateMovieStatsTableOnWatchlistDelete::class
        ],
        MoobSaved::class => [
            UpdateMovieStatsTableOnMoobSave::class
        ],
        MoobDeleted::class => [
            UpdateMovieStatsTableOnMoobDelete::class
        ],
        UserFollowsCreated::class => [
            SendPushOnUserFollowsCreated::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
