<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSpoilsMoob extends Model
{
    protected $table = 'user_spoils_moob';

    protected $fillable = ['user_id', 'moob_id'];
}
