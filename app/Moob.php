<?php

namespace App;

use App\Events\MoobDeleted;
use App\Events\MoobSaved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Moob extends Model
{
    protected $table = 'moobs';

    protected $dispatchesEvents = [
        'saved' => MoobSaved::class,
        'deleted' => MoobDeleted::class
    ];

    protected $fillable = [
        'user_id',
        'film_id',
        'date',
        'comment',
        'score'
    ];

    protected $appends = ['likes_count'];

    public function user()
    {
        return $this->belongsTo( User::class);
    }

    public function likes()
    {
        return $this->hasMany(UserLikesMoob::class);
    }

    public function spoilers()
    {
        return $this->hasMany(UserSpoilsMoob::class);
    }

    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }
}
