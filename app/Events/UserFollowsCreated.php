<?php

namespace App\Events;

use App\UserFollows;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserFollowsCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $follow;

    /**
     * Create a new event instance.
     *
     * @param UserFollows $follow
     */
    public function __construct(UserFollows $follow)
    {
        $this->follow = $follow;
    }
}
