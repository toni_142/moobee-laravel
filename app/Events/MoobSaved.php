<?php

namespace App\Events;

use App\Moob;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MoobSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $moob;

    /**
     * Create a new event instance.
     *
     * @param Moob $moob
     */
    public function __construct(Moob $moob)
    {
        $this->moob = $moob;
    }
}
