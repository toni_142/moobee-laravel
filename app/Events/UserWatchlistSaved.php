<?php

namespace App\Events;

use App\UserWatchlist;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserWatchlistSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $watchlist;

    /**
     * Create a new event instance.
     *
     * @param UserWatchlist $watchlist
     */
    public function __construct(UserWatchlist $watchlist)
    {
        $this->watchlist = $watchlist;
    }
}
