<?php

namespace App;

use App\Events\UserWatchlistDeleted;
use App\Events\UserWatchlistSaved;
use Illuminate\Database\Eloquent\Model;

class UserWatchlist extends Model
{
    protected $fillable = [
        'user_id', 'film_id', 'has_liked', 'is_favorite'
    ];

    protected $dispatchesEvents = [
        'saved' => UserWatchlistSaved::class,
        'deleted' => UserWatchlistDeleted::class
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
