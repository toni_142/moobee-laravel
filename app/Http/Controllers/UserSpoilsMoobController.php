<?php

namespace App\Http\Controllers;

use App\Moob;
use App\User;
use App\UserSpoilsMoob;
use Illuminate\Http\Request;

class UserSpoilsMoobController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user = User::find(auth()->id());

        $spoiler = new UserSpoilsMoob([
            'user_id' => $user->id,
            'moob_id' => intval($id)
        ]);

        $spoiler->save();

        return response()->json(
            [
                'error' => false,
                'data' => $spoiler
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $moob = Moob::find($id);

        if (!$moob) {
            return response()->json(
                [
                    'error' => false,
                    'data' => 'No se ha encontrado el moob ' . $id
                ],
                200
            );
        }

        $query = UserSpoilsMoob::query()
            ->where('moob_id', $moob->id);

        $total = $query->count();
        $spoilers = $query->get();

        return response()->json(
            [
                'error' => false,
                'data' => $spoilers,
                'total' => $total
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find(auth()->id());

        try {
            $spoiler = UserSpoilsMoob::query()
                ->where('moob_id', $id)
                ->where('user_id', $user->id)
                ->first();

            if (!$spoiler) {
                return response()->json(
                    [
                        'error' => true,
                        'data' => 'El moob no se había marcado como spoiler'
                    ],
                    402
                );
            }

            $spoiler->delete();
        } catch (\Exception $exception) {
            return response()->json(
                [
                    'error' => true,
                    'data' => $exception->getMessage()
                ],
                500
            );
        }

        return response()->json(
            [
                'error' => false,
                'data' => 'Moob desmarcado como spoiler'
            ],
            200
        );
    }
}
