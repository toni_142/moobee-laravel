<?php

namespace App\Http\Controllers;

use App\Moob;
use App\User;
use App\UserWatchlist;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *      title="Moobee API",
     *      version="0.1",
     *      @OA\Contact(
     *          name="Toni García Alhambra",
     *          email="toni_142@icloud.com"
     *      )
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function getHomeInfo(Request $request)
    {
        $moobs_sent = Moob::query()->count();
        $users_registered = User::query()->count();
        $movies_seen = UserWatchlist::query()->count();
        
        return response()->json([
            'error' => false,
            'data' => [
                'movies_seen' => $movies_seen,
                'moobs_sent' => $moobs_sent,
                'users_registered' => $users_registered
            ]
        ]);
    }
}
