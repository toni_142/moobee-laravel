<?php

namespace App\Http\Controllers;

use App\MoviesStats;
use Illuminate\Http\Request;

class MoviesStatsController extends Controller
{
    public function show($id)
    {
        $stats = MoviesStats::query()
            ->where('film_id', $id)
            ->first();

        if (!$stats) {
            return response()->json(
                [
                    'error' => false,
                    'data' => [
                        'film_id' => intval($id),
                        'times_seen' => 0,
                        'moobs_sent' => 0,
                        'total_likes' => 0
                    ]
                ],
                200
            );
        }

        return response()->json(
            [
                'error' => false,
                'data' => $stats
            ],
            200
        );
    }
}
