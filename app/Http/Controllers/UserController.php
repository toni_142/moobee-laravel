<?php

namespace App\Http\Controllers;

use App\Http\Resources\MoobResource;
use App\Http\Resources\UserListResource;
use App\Http\Resources\UserProfile;
use App\Jobs\DeleteDraftUser;
use App\User;
use App\UserWatchlist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 * @package App\Http\Controllers
 *
 * @OA\Schema(
 *      schema="UserSchema",
 *      type="object",
 *      description="User model",
 *      @OA\Property(
 *          property="id",
 *          type="integer",
 *          example=63
 *      ),
 *      @OA\Property(
 *          property="username",
 *          type="string",
 *          example="xavier"
 *      ),
 *      @OA\Property(
 *          property="first_name",
 *          type="string",
 *          example="Xavier"
 *      ),
 *      @OA\Property(
 *          property="bio",
 *          type="string",
 *          example="Lorem ipsum dolor sit amet..."
 *      ),
 *      @OA\Property(
 *          property="avatar",
 *          type="string",
 *          example="789546231-xavier.jpg"
 *      ),
 *      @OA\Property(
 *          property="header_image",
 *          type="string",
 *          example="789654123-film.jpg"
 *      ),
 *      @OA\Property(
 *          property="is_draft",
 *          type="boolean",
 *          example=false
 *      ),
 * )
 */

class UserController extends Controller
{
    protected $items_per_page = 10;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *      path="/register",
     *      summary="Register a new user",
     *      operationId="api.user.register",
     *      tags={"User"},
     *      @OA\Parameter(
     *          name="first_name",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="username",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          ),
     *      ),
     *
     *      @OA\Response(
     *          response=200,
     *          description="User registered successfully",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="boolean", example=false),
     *              @OA\Property(property="data", type="string", ref="#/components/schemas/UserSchema"),
     *          )
     *      ),
     * )
     */
    public function register(Request $request)
    {
        $rules = [
            'email' => 'unique:users',
            'username' => 'required',
            'first_name' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->errors()->count()) {
            return response()->json(
                [
                    'error' => true,
                    'data' => $validator->errors()->toArray()
                ],
                400
            );
        }

        if (($request->has('source')) && ($request->get('source') == 'web')) {
            $rules = [
                'email' => 'unique:users',
                'username' => 'required|unique:users',
                'first_name' => 'required',
                'password' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->errors()->count()) {
                return response()->json(
                    [
                        'error' => true,
                        'data' => $validator->errors()->toArray()
                    ],
                    400
                );
            }

            $user = new User([
                'username' => $request->get('username'),
                'first_name' => $request->get('first_name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'is_draft' => false
            ]);

            $user->save();

            return response()->json(
                [
                    'error' => false,
                    'data' => [
                        'user' => $user,
                        'token' => $user->createToken('register')->accessToken
                    ]
                ],
                200
            );
        }

        if (!$request->has('email') && !$request->has('password')) {
            $existing_user = (bool) User::query()
                ->where('username', $request->get('username'))
                ->first();

            if ($existing_user) {
                return response()->json(
                    [
                        'error' => true,
                        'data' => 'El nombre de usuario ya está cogido'
                    ],
                    409
                );
            }

            $user = new User([
                'username' => $request->get('username'),
                'first_name' => $request->get('first_name'),
                'is_draft' => true
            ]);

            $user->save();

            $job = (new DeleteDraftUser($user))->delay(Carbon::now()->addMinutes(15));
            $this->dispatch($job);

            return response()->json(
                [
                    'error' => false,
                    'data' => $user
                ]
            );
        } else {
            $password = Hash::make($request->get('password'));

            $user_data = [
                'email' => $request->get('email'),
                'password' => $password,
                'bio' => $request->get('bio', null),
                'is_draft' => false
            ];

            $user = User::query()
                ->where('username', $request->get('username'))
                ->first();

            if ($user) {
                $user->fill($user_data);
                $user->save();

                return response()->json(
                    [
                        'error' => false,
                        'data' => [
                            'user' => $user,
                            'token' => $user->createToken('register')->accessToken
                        ]
                    ],
                    200
                );

            } else {
                return response()->json(
                    [
                        'error' => true,
                        'data' => 'El usuario no existe'
                    ],
                    404
                );
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *      path="/login",
     *      summary="Login a new user",
     *      operationId="api.user.login",
     *      tags={"User"},
     *      @OA\Parameter(
     *          name="username",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="email",
     *          in="query",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          ),
     *      ),
     *      @OA\Parameter(
     *          name="password",
     *          in="query",
     *          required=true,
     *          @OA\Schema(
     *              type="string",
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="User logged in successfully",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="boolean", example=false),
     *              @OA\Property(property="data", type="string", ref="#/components/schemas/UserSchema"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Contraseña incorrecta",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="boolean", example=true),
     *              @OA\Property(property="data", type="string", example="Contraseña incorrecta"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Usuario o correo incorrecto",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="boolean", example=true),
     *              @OA\Property(property="data", type="string", example="Email o username inválido"),
     *          )
     *      ),
     * )
     */
    public function login(Request $request)
    {
        $field = $request->has('email') ? 'email' : 'username';
        $value = $request->has('email') ? $request->get('email') : $request->get('username');

        $credentials = [
            $field => $value,
            'password' => $request->get('password')
        ];

        if (auth()->attempt($credentials)) {
            $user = User::query()
                ->where($field, $value)
                ->first();

            return response()->json(
                [
                    'error' => false,
                    'data' => [
                        'user' => $user,
                        'token' => $user->createToken('login')->accessToken
                    ]
                ],
                200
            );
        } else {
            if (User::query()->where($field, $value)->first()) {
                return response()->json(
                    [
                        'error' => true,
                        'data' => 'Contraseña incorrecta'
                    ],
                    400
                );
            } else {
                return response()->json(
                    [
                        'error' => true,
                        'data' => 'Email o username inválido'
                    ],
                    404
                );
            }

        }
    }

    public function show($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No se ha encontrado al usuario'
                ],
                404
            );
        }

        return response()->json(
            [
                'error' => false,
                'data' => UserProfile::make($user)
            ]
        );
    }

    public function getUserWatchlist(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No se ha encontrado el usuario ' . $id
                ],
                404
            );
        }

        $page = $request->get('page', 0);
        $take = $request->get('take', $this->items_per_page);

        $user_watchlist = $user->watchlist()->select('film_id')->get()->toArray();
        $total = $user->watchlist()->count();

        return response()->json(
            [
                'error' => false,
                'n_pages' => ceil($total / $take ),
                'data' => array_column($user_watchlist, 'film_id')
            ]
        );
    }

    public function getUserMoobs(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No se ha encontrado el usuario ' . $id
                ],
                404
            );
        }

        $page = $request->get('page', 0);
        $take = $request->get('take', $this->items_per_page);

        $user_moobs = $user->moobs()->take($take)->skip($take * $page)->get();
        $total = $user->moobs()->count();

        return response()->json(
            [
                'error' => false,
                'n_pages' => ceil($total / $take ),
                'data' => MoobResource::collection($user_moobs)
            ]
        );
    }

    public function checkMoobsByFilmId($id)
    {
        $user = User::find(auth()->id());

        $moobs = $user->moobs()->where('film_id', $id)->count();

        return response()->json(
            [
                'error' => false,
                'data' => (bool) $moobs
            ]
        );
    }

    public function search(Request $request, $query)
    {
        $q = User::query()
            ->selectRaw('*, MATCH (username,first_name) AGAINST(? IN NATURAL LANGUAGE MODE) AS score', [$query])
            ->whereRaw('MATCH(first_name, username) AGAINST(? IN BOOLEAN MODE)', ['*' . $query . '*'])
            ->orderBy('score', 'desc');

        $total = $q->count();

        $page = $request->get('page', 0);
        $take = $request->get('take', $this->items_per_page);

        $q->take($take);
        $q->skip($take * $page);

        $users = $q->get();

        return response()->json(
            [
                'error' => false,
                'data' => UserListResource::collection($users),
                'total' => $total
            ],
            200
        );
    }

    public function getFollowersWhoLikedFilm(Request $request, $id)
    {
        $user = User::find(auth()->id());

        $query = User::query()
            ->whereHas('followers', function ($q) use ($user) {
                $q->where('follower_id', $user->id);
            })
            ->whereHas('watchlist', function ($q) use ($id) {
                $q->where('has_liked', true);
                $q->where('film_id', $id);
            });

        $total = $query->count();

        $page = $request->get('page', 0);
        $take = $request->get('take', $this->items_per_page);

        $query->take($take);
        $query->skip($take * $page);

        $followings = $query->get();

        return response()->json(
            [
                'error' => false,
                'data' => UserListResource::collection($followings),
                'total' => $total
            ],
            200
        );
    }

    public function getFollowersWhoWatchedFilm(Request $request, $id)
    {
        $user = User::find(auth()->id());

        $query = User::query()
            ->whereHas('followers', function ($q) use ($user) {
                $q->where('follower_id', $user->id);
            })
            ->whereHas('watchlist', function ($q) use ($id) {
                $q->where('film_id', $id);
            });

        $total = $query->toSql();

        $page = $request->get('page', 0);
        $take = $request->get('take', $this->items_per_page);

        $query->take($take);
        $query->skip($take * $page);

        $followings = $query->get();

        return response()->json(
            [
                'error' => false,
                'data' => UserListResource::collection($followings),
                'total' => $total
            ],
            200
        );
    }
}
