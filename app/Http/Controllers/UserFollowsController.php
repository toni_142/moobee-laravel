<?php

namespace App\Http\Controllers;

use App\User;
use App\UserFollows;
use Illuminate\Http\Request;

class UserFollowsController extends Controller
{
    public function follow($id)
    {
        $user = User::find(auth()->id());

        if ($user->following()->where('follower_id', $id)->first()) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'Ya sigues a este usuario'
                ],
                401
            );
        }

        $follow = new UserFollows([
            'user_id' => $user->id,
            'follower_id' => $id
        ]);

        $follow->save();

        return response()->json(
            [
                'error' => false,
                'data' => $follow
            ]
        );
    }

    public function unfollow($id)
    {
        $user = User::find(auth()->id());

        $follow = $user->following()
            ->where('follower_id', $id)
            ->delete();

        if (!$follow) {
            return response()->json(
                [
                    'error' => true,
                    'follow' => $follow,
                    'data' => 'No se ha podido dejar de seguir al usuario'
                ],
                500
            );
        }

        return response()->json(
            [
                'error' => false,
                'data' => 'Se ha dejado de seguir al usuario correctamente'
            ]
        );
    }
}
