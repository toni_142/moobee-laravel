<?php

namespace App\Http\Controllers;

use App\User;
use App\UserWatchlist;
use Illuminate\Http\Request;

class WatchlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(auth()->id());

        return response()->json(
            [
                'error' => false,
                'data' => $user->watchlist
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(auth()->id());

        $existing_film = UserWatchlist::query()
            ->where([
                'user_id' => $user->id,
                'film_id' => $request->film_id
            ])
            ->first();

        if ($existing_film) {
            return response()->json(
                [
                    'error' => false,
                    'data' => 'La película ya está marcada como vista'
                ],
                400
            );
        }

        $watchlist = new UserWatchlist([
            'user_id' => $user->id,
            'film_id' => $request->film_id,
        ]);

        $watchlist->save();

        return response()->json(
            [
                'error' => false,
                'data' => $watchlist
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find(auth()->id());

        $film = UserWatchlist::query()
            ->where([['user_id', $user->id], ['film_id', $id]])
            ->first();

        return response()->json(
            [
                'error' => false,
                'data' => $film
            ],
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find(auth()->id());

        $film = UserWatchlist::query()
            ->where([['user_id', $user->id], ['film_id', $id]])
            ->first();

        $film->fill($request->all());
        $film->save();

        return response()->json(
            [
                'error' => false,
                'data' => $film
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user = User::find(auth()->id());

        $film = UserWatchlist::query()
            ->where([['user_id', $user->id], ['film_id', $id]])
            ->first();

        if (!$film) {
            return response()->json(
                [
                    'error' => false,
                    'data' => 'Film not found in Watchlist'
                ],
                404
            );
        }

        $film->delete();

        $user->moobs()
            ->where('film_id', $id)
            ->delete();

        return response()->json(
            [
                'error' => false,
                'data' => 'Film deleted successfully'
            ],
            200
        );
    }
}
