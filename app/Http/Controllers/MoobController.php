<?php

namespace App\Http\Controllers;

use App\Http\Resources\MoobResource;
use App\Moob;
use App\User;
use App\UserLikesMoob;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MoobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moobs = Moob::all();

        return response()->json(
            [
                'error' => false,
                'data' => $moobs
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(auth()->id());

        $last_moob = $user->moobs()->orderBy('created_at', 'desc')->first();
        $minutes_from_last_moob = 1000;
        $minutes_to_new_moob = 60;

        if ($last_moob) {
            $minutes_from_last_moob = Carbon::createFromTimeString($last_moob->created_at)->diffInMinutes(Carbon::now());
        }

        $minutes_to_unlock = ($minutes_to_new_moob - $minutes_from_last_moob);

        if ($minutes_from_last_moob < $minutes_to_new_moob) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'Solo se puede enviar un moob cada hora (' . $minutes_to_unlock . ' minutos restantes).'
                ],
                400
            );
        }

        $moob = new Moob([
            'user_id' => $user->id,
            'film_id' => $request->get('film_id'),
            'comment' => $request->get('comment', null),
            'score' => $request->get('score')
        ]);

        $moob->save();

        return response()->json(
            [
                'error' => false,
                'data' => $moob
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $moob = Moob::find($id);

        if (!$moob) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No se ha encontrado el moob'
                ],
                404
            );
        }

        return response()->json(
            [
                'error' => false,
                'data' => $moob
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $moob = Moob::find($id);

        if (!$moob) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No se ha podido encontrar el moob'
                ],
                404
            );
        }

        $moob->delete();

        return response()->json(
            [
                'error' => false,
                'data' => $moob
            ]
        );
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function canSendMoob()
    {
        $user = User::find(auth()->id());

        $number_of_moobs_sent = $user->moobs()->count();

        if ($number_of_moobs_sent > intval(env('NUMBER_OF_MOOBS_WITHOUT_TIME_LIMIT'))) {
            $last_moob = $user->moobs()->orderBy('created_at', 'desc')->first();
            $minutes_from_last_moob = 1000;
            $minutes_to_new_moob = 60;

            if ($last_moob) {
                $minutes_from_last_moob = Carbon::createFromTimeString($last_moob->created_at)->diffInMinutes(Carbon::now());
            }

            $minutes_to_unlock = ($minutes_to_new_moob - $minutes_from_last_moob);

            if ($minutes_from_last_moob < $minutes_to_new_moob) {
                return response()->json(
                    [
                        'error' => true,
                        'data' => 'Solo se puede enviar un moob cada hora (' . $minutes_to_unlock . ' minutos restantes).'
                    ],
                    400
                );
            }
        }

        return response()->json(
            [
                'error' => false,
                'data' => 'Ya puedes enviar un nuevo moob'
            ],
            200
        );
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFilmMoobs($id)
    {
        $moobs = Moob::query()
            ->where([
                'film_id' => $id,
            ])
            ->withCount(['likes as like_count' => function ($query) {
                $query->select(DB::raw('count(*)'));
            }])
            ->orderByDesc('like_count')
            ->get();

        if ($moobs) {
            return response()->json(
                [
                    'error' => false,
                    'data' => $moobs
                ],
                200
            );
        }

        return response()->json(
            [
                'error' => false,
                'data' => false
            ],
            200
        );
    }

    public function getFollowingMoobs(Request $request)
    {
        $user = User::find(auth()->id());

        $following_ids = array_column($user->following()->select('follower_id')->get()->toArray(), 'follower_id');
        array_push($following_ids, $user->id);

        $page = $request->get('page', 0);
        $take = $request->get('take', 10);

        $moobs = Moob::query()
            ->whereIn('user_id', $following_ids)
            ->take($take)
            ->skip($take * $page)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json(
            [
                'error' => false,
                'data' => MoobResource::collection($moobs),
                'page' => $page
            ],
            200
        );
    }

    public function likeMoob($id)
    {
        $user = User::find(auth()->id());

        $moob = Moob::find($id);

        if (!$moob) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No se ha encontrado el moob'
                ],
                404
            );
        }

        $existing_like = UserLikesMoob::query()
            ->where([
                'user_id' => $user->id,
                'moob_id' => $moob->id
            ])
            ->first();

        if ($existing_like) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'Ya le has dado like a este moob'
                ],
                401
            );
        }

        $like = new UserLikesMoob([
            'user_id' => $user->id,
            'moob_id' => $moob->id
        ]);

        $like->save();

        return response()->json(
            [
                'error' => false,
                'data' => $like
            ]
        );
    }

    public function unlikeMoob($id)
    {
        $user = User::find(auth()->id());

        $moob = Moob::find($id);

        if (!$moob) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No se ha encontrado el moob'
                ],
                404
            );
        }

        $existing_like = UserLikesMoob::query()
            ->where([
                'user_id' => $user->id,
                'moob_id' => $moob->id
            ])
            ->first();

        if (!$existing_like) {
            return response()->json(
                [
                    'error' => true,
                    'data' => 'No le has dado like a este moob'
                ],
                401
            );
        }

        $existing_like->delete();

        return response()->json(
            [
                'error' => false,
                'data' => 'Like eliminado correctamente'
            ]
        );
    }
}
