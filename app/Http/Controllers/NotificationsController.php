<?php

namespace App\Http\Controllers;

use App\Http\Resources\NotificationResource;
use App\User;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function index(Request $request)
    {
        $user = User::find(auth()->id());
        return response()->json([
            'error' => false,
            'data' => NotificationResource::collection($user->notifications)
        ]);

    }
}
