<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $subject = null;
        $content = null;

        switch ($this->type) {
            case 'follow':
                $subject = 'Nuevo seguidor';
                $content = '¡@' . $this->userFrom->username . ' ha comenzado a seguirte!';
        }

        return [
            'user_from' => $this->userFrom()->select('id', 'first_name', 'username', 'avatar')->first(),
            'subject' => $subject,
            'content' => $content,
            'date' => $this->created_at->toDateTimeString(),
        ];
    }
}
