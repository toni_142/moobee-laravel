<?php

namespace App\Http\Resources;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class MoobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find(auth()->id());

        App::setLocale('es');

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'film_id' => $this->film_id,
            'comment' => $this->comment,
            'score' => $this->score,
            'likes_count' => $this->likes_count,
            'spoilers_count' => $this->spoilers()->count(),
            'date' => $this->created_at->toDateTimeString(),
            'user_username' => $this->user->username,
            'user_first_name' => $this->user->first_name,
            'user_avatar' => $this->user->avatar,
            'is_liked' => $this->when($user->likes()->where('moob_id', $this->id)->first(), true, false),
            'has_seen_movie' => (bool) $user->watchlist()->where('film_id', $this->film_id)->first(),
            'has_marked_as_spoiler' => $this->when($user->spoilers()->where('moob_id', $this->id)->first(), true, false)
        ];
    }
}
