<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find(auth()->id());
        return [
            'profile_image' => $this->avatar,
            'name' => $this->first_name,
            'username' => $this->username,
            'following' => $this->following()->count(),
            'followers' => $this->followers()->count(),
            'bio' => $this->bio,
            'n_moobs' => $this->moobs()->count(),
            'n_films_watched' => $this->watchlist()->count(),
            'n_lists' => 0,
            'last_moob' => MoobResource::make($this->moobs()->orderBy('created_at', 'desc')->first()),
            'last_three_films' => $this->watchlist()->orderBy('created_at', 'desc')->limit(3)->get(),
            'followed' => $this->when($user->following()->where('follower_id', $this->id)->first(), true, false),
            'is_own_profile' => $this->when($this->id == $user->id, true, false)
        ];
    }
}
