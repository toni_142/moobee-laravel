<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLikesMoob extends Model
{
    protected $table = 'user_likes_moob';

    protected $fillable = [
        'user_id',
        'moob_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function moob()
    {
        return $this->belongsTo(Moob::class);
    }
}
