<?php

namespace App\Listeners;

use App\Events\MoobDeleted;
use App\MoviesStats;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateMovieStatsTableOnMoobDelete
{
    /**
     * Handle the event.
     *
     * @param MoobDeleted $event
     * @return void
     */
    public function handle(MoobDeleted $event)
    {
        $film_id = $event->moob->film_id;

        $stats = MoviesStats::query()
            ->where('film_id', $film_id)
            ->first();

        if (!$stats) {
            $new_stats = new MoviesStats([
                'film_id' => $film_id,
                'times_seen' => 1,
                'moobs_sent' => 0
            ]);
            $new_stats->save();
        } else {
            $stats->decrement('moobs_sent');
        }
    }
}
