<?php

namespace App\Listeners;

use App\Events\UserWatchlistSaved;
use App\MoviesStats;

class UpdateMovieStatsTableOnWatchlistSave
{
    /**
     * Handle the event.
     *
     * @param UserWatchlistSaved $event
     * @return void
     */
    public function handle(UserWatchlistSaved $event)
    {
        $is_new = $event->watchlist->wasRecentlyCreated;
        $film_id = $event->watchlist->film_id;

        if ($is_new) {
            $stats = MoviesStats::query()
                ->where('film_id', $film_id)
                ->first();

            if (!$stats) {
                $new_stats = new MoviesStats([
                    'film_id' => $film_id,
                    'times_seen' => 1
                ]);
                $new_stats->save();
            } else {
                $stats->increment('times_seen');
            }
        } else {
            if ($event->watchlist->isDirty('has_liked')) {
                $method = $event->watchlist->has_liked ? 'increment' : 'decrement';
                $stats = MoviesStats::query()
                    ->where('film_id', $film_id)
                    ->first();

                if (!$stats) {
                    $new_stats = new MoviesStats([
                        'film_id' => $film_id,
                        'times_seen' => 1,
                        'total_likes' => 1
                    ]);
                    $new_stats->save();
                } else {
                    if ($stats->total_likes || $method == 'increment') {
                        $stats->$method('total_likes');
                    }
                }
            }
        }
    }
}
