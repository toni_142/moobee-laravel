<?php

namespace App\Listeners;

use App\Events\MoobSaved;
use App\MoviesStats;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateMovieStatsTableOnMoobSave implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param MoobSaved $event
     * @return void
     */
    public function handle(MoobSaved $event)
    {
        $film_id = $event->moob->film_id;

        $stats = MoviesStats::query()
            ->where('film_id', $film_id)
            ->first();

        if (!$stats) {
            $new_stats = new MoviesStats([
                'film_id' => $film_id,
                'times_seen' => 1,
                'moobs_sent' => 1
            ]);
            $new_stats->save();
        } else {
            $stats->increment('moobs_sent');
        }

    }
}
