<?php

namespace App\Listeners;

use App\Events\UserFollowsCreated;
use App\Notification;
use App\Notifications\NewFollowerPush;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPushOnUserFollowsCreated
{
    /**
     * Handle the event.
     *
     * @param UserFollowsCreated $event
     * @return void
     */
    public function handle(UserFollowsCreated $event)
    {
        $notification = new Notification([
            'user_from_id' => $event->follow->follower->id,
            'user_to_id' => $event->follow->followed->id,
            'type' => 'follow',
        ]);

        $notification->save();

        $event->follow->followed->notify(new NewFollowerPush());
    }
}
