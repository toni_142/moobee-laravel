<?php

namespace App\Listeners;

use App\Events\UserWatchlistDeleted;
use App\MoviesStats;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateMovieStatsTableOnWatchlistDelete
{
    /**
     * Handle the event.
     *
     * @param UserWatchlistDeleted $event
     * @return void
     */
    public function handle(UserWatchlistDeleted $event)
    {
        $film_id = $event->watchlist->film_id;

        $stats = MoviesStats::query()
            ->where('film_id', $film_id)
            ->first();

        if (!$stats) {
            $new_stats = new MoviesStats([
                'film_id' => $film_id,
            ]);
            $new_stats->save();
        } else {
            $stats->decrement('times_seen');
            $stats->decrement('total_likes');
        }
    }
}
