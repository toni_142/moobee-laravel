<?php

namespace App;

use App\Events\UserFollowsCreated;
use Illuminate\Database\Eloquent\Model;

class UserFollows extends Model
{
    protected $table = 'user_follows';

    protected $fillable = ['user_id', 'follower_id'];

    protected $dispatchesEvents = [
        'created' => UserFollowsCreated::class
    ];

    public function follower()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function followed()
    {
        return $this->belongsTo(User::class, 'follower_id');
    }
}
