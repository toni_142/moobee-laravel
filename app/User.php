<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * Array of devices to send notifications.
     *
     * @return array
     */
    public function routeNotificationForOneSignal()
    {
        return array_column($this->devices()->select('device_id')->get()->toArray(), 'device_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'email', 'password', 'bio', 'avatar', 'header_image', 'is_draft'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function watchlist()
    {
        return $this->hasMany(UserWatchlist::class);
    }

    public function moobs()
    {
        return $this->hasMany(Moob::class);
    }

    public function followers()
    {
        return $this->hasMany(UserFollows::class, 'follower_id');
    }

    public function following()
    {
        return $this->hasMany(UserFollows::class, 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(UserLikesMoob::class);
    }

    public function spoilers()
    {
        return $this->hasMany(UserSpoilsMoob::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'user_to_id');
    }

    public function devices()
    {
        return $this->hasMany(UserDevice::class);
    }
}
