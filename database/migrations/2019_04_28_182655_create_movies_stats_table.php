<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies_stats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('film_id')->index();
            $table->integer('times_seen')->default(0);
            $table->integer('moobs_sent')->default(0);
            $table->integer('total_likes')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies_stats');
    }
}
