<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWatchlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_watchlists', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('film_id');
            $table->boolean('has_liked')->nullable();
            $table->boolean('is_favorite')->nullable();
            $table->timestamps();
            $table->primary(['user_id', 'film_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_watchlists');
    }
}
